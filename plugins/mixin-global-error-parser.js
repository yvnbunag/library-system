import Vue from 'vue'

Vue.mixin({
  methods: {
    customErrorParser (err) {
      if(err.response !== undefined) {
        if ('data' in err.response) {
          if ('reason' in err.response.data) {
            if (err.response.data.reason === 'authentication mismatch') {
              this.$cookies.set('forceLogoutMessage', 'User credentials updated')
              return this.$auth.logout()
            }
            if (err.response.data.reason === 'status credential revoked') {
              this.$cookies.set('forceLogoutMessage', 'User account has been disabled')
              return this.$auth.logout()
            }
            if (err.response.data.reason === 'Invalid user status') {
              this.$cookies.set('forceLogoutMessage', 'User account status invalid')
              return this.$auth.logout()
            }
            return err.response.data.reason
          }
        }
      }
      return err.message
    }
  }
})