require('dotenv').config()
const bodyParser = require('body-parser')

const { description } = require('./package')
const api = require('./api')

const {
  APPLICATION_URL,
  API_URL,
  PREVIEW_MODE,
  TEST_CREDENTIALS_LINK,
  STACK_LINK,
} = process.env

module.exports = {
  mode: 'universal',
  head: {
    title: 'Library System',
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: description }
    ],
    script: [
      { src: `${APPLICATION_URL}/integrated/vendor/jquery/jquery-3.2.1.min.js` },
      { src: `${APPLICATION_URL}/integrated/vendor/bootstrap/js/bootstrap.bundle.min.js` }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  css: [
    '~/assets/css/general.css',
    '~/assets/integrated/vendor/bootstrap/css/bootstrap.min.css',
    '~/assets/integrated/css/heroic-features.css'
  ],

  loading: {
    color: '#6CC4EE',
    height: '4px'
  },
  plugins: [
    {
      src: '~plugins/vue-js-modal',
      ssr: false
    },
    {
      src: '~plugins/vue-notification',
      ssr: false
    },
    {
      src: '~plugins/vue-moment',
      ssr: false
    },
    {
      src: '~plugins/mixin-global-error-parser',
      ssr: false
    },
    {
      src: '~plugins/vue-paginate.js',
      ssr: false
    }
  ],
  modules: [
    '@nuxtjs/axios',
    '@nuxtjs/auth',
    'cookie-universal-nuxt'
  ],
  env: {
    API_URL,
    APPLICATION_URL,
    PREVIEW_MODE,
    TEST_CREDENTIALS_LINK,
    STACK_LINK,
  },
  serverMiddleware: [
    bodyParser.json(),
    api
  ],
  router: {
    middleware: ['auth']
  },
  axios: { baseURL: API_URL || 'http://localhost:3010' },
  auth: {
    strategies: {
      local: {
        endpoints: {
          login: {
            url: `${APPLICATION_URL}/api/login`,
            method: 'post',
            propertyName: 'bearerToken'
          },
          user: {
            url: `users/settings`,
            method: 'get',
            propertyName: 'data'
          },
          logout: false
        }
      }
    },
    redirect: {
      login: '/login',
      logout: '/login',
      home: '/'
    }
  },
  build: {
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    },
    babel:{
      plugins: [
        ["@babel/plugin-proposal-class-properties", { "loose": true }],
        ["@babel/plugin-proposal-private-methods", { "loose": true }],
        ["@babel/plugin-proposal-private-property-in-object", { "loose": true }]
      ]
    }
  }
}
