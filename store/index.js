export const strict = false

export const getters = {
  isAuthenticated(state) {
    return state.auth.loggedIn
  },

  loggedInUser(state) {
    return state.auth.user
  }
}

export const mutations = {
  incrementBorrowCount(state) {
    state.auth.user.borrows += 1
  },

  decrementBorrowCount(state) {
    state.auth.user.borrows -= 1
  },

  modifyBorrowCount(state, borrowCount) {
    state.auth.user.borrows = borrowCount
  },

  async refetchSettings(state) {
    try {
      if(!state.auth.dataRefetched || state.auth.dataRefetched === undefined) {
        state.auth.dataRefetched = true
        const updatedSettings = await this.$axios.get('users/settings')
        state.auth.user = updatedSettings.data.data
        state.auth.dataRefetched = false
      }
    } catch (err) {
      console.error(err)
      state.auth.dataRefetched = false
    }
  }
}

export const actions = {
  incrementBorrowCount({ commit }) {
    commit('incrementBorrowCount')
  },

  decrementBorrowCount({ commit }) {
    commit('decrementBorrowCount')
  },

  modifyBorrowCount({ commit }, borrowCount) {
    commit('modifyBorrowCount', borrowCount)
  },

  refetchSettings({ commit }){
    commit('refetchSettings')
  }
}