export const disableWindowScroll = () => {
  let marginRightPx = 0;
    if(window.getComputedStyle) {
      let bodyStyle = window.getComputedStyle(document.body);
      if(bodyStyle) {
          marginRightPx = parseInt(bodyStyle.marginRight, 10);
      }
    }

    let scrollbarWidthPx = window.innerWidth - document.body.clientWidth;
    Object.assign(document.body.style, {
      overflow: 'hidden',
      marginRight: `${marginRightPx + scrollbarWidthPx}px`
    })
    document.documentElement.style.overflow = 'hidden'
}

export const enableWindowScroll = () => {
  Object.assign(document.body.style, {
      overflow: 'hidden',
      marginRight: `0px`
  })
  document.documentElement.style.overflow = 'auto'
}


export default {
  disableWindowScroll,
  enableWindowScroll
}