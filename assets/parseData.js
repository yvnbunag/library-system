import credentialsConfig from '~/config/credentials.mapping' 
import borrowSettings from '~/config/borrow.settings'
import moment from 'moment'

export const jsUcFirst = (string) => {
  if(string !== '' && string !== undefined && string !== null) {
    return string.charAt(0).toUpperCase() + string.slice(1)
  }
  return null
}

export const parseSelectOption = (option) => {
  var option = option.charAt(0).toUpperCase() + option.slice(1)
  option = option.replace('_',' ')
  return option
}

export const formatDate = (date,time = false) => {
  if (time) {
    return moment(date).utcOffset("+08:00").format("MMMM DD, YYYY hh:mma")
  }
  return moment(date).utcOffset("+08:00").format("MMMM DD, YYYY") 
}

export const formatExpiryDate = (date,time = false) => {
  if (time) {
    return moment(date).add(borrowSettings.BORROW_REQUEST_EXPIRY_IN_DAYS, 'days')
      .utcOffset("+08:00").format("MMMM DD, YYYY hh:mma")
  }
  return moment(date).add(borrowSettings.BORROW_REQUEST_EXPIRY_IN_DAYS, 'days')
    .utcOffset("+08:00").format("MMMM DD, YYYY") 
}

export const formatTimeStamp = (date,time = false) => {
  if (time) {
    return moment.unix(date).format("MMMM DD, YYYY hh:mma")
  }
  return moment.unix(date).format("MMMM DD, YYYY")
}

export const formatUntilDate = (date,time = false) => {
  if (time) {
    return moment.unix(date).format("MMMM DD, YYYY hh:mma")
  }
  return moment.unix(date).format("MMMM DD, YYYY")
}
 
export const getBorrowDateValidity = (startDate) => {
  const start = moment(new Date()).format('X')
  const end = moment.unix(startDate).format('X')
  return computeDateValidity(start, end)
}

export const getFinishedBorrowDateValidity = (startDate, endDate) => {
  const start = moment.unix(startDate).format('X')
  const end = moment.unix(endDate).format('X')
  return computeDateValidity(start, end)
}

const computeDateValidity = (start, end) => {
  const diff = end - start
  const duration = moment.duration(diff, 'seconds')


  const isValid = duration.asDays() >= 0
  
  const hours = Math.ceil(Math.abs(duration.asHours()))
  const days = Math.ceil(Math.abs(duration.asDays()))

  let validity

  if (isValid) {
    const remainingDays = hours < 24
      ? hours + ' hour/s'
      : days + ' day/s'

    validity = {
      isValid,
      remainingDays
    }
  } else {
    const overdueTime = hours < 24 
      ? hours + ' hour/s'
      : days + ' day/s'
    const penalty = `₱ ${(days*borrowSettings.LENT_RESOURCE_PENALTY_IN_PESO)
      .toFixed(2,10)}`

    validity = {
      isValid,
      penalty,
      overdueTime
    }
  }

  return validity
}

const computeValidityDuration = (start, end) => {
  const diff = end - start
  const duration = moment.duration(diff, 'seconds')

  if (duration.asDays() > 1) {
    return `(${Math.ceil(duration.asDays())} days)`
  }
  return `(${Math.ceil(duration.asDays())} day)`
}

export const validateAdmin = (type) => {
  let isAdmin = false

  credentialsConfig.type.admin.forEach((value) => {
    if (value === type) {
      isAdmin = true
    }
  })

  return isAdmin
}

export default {
  jsUcFirst,
  parseSelectOption,
  formatDate,
  formatExpiryDate,
  formatTimeStamp,
  formatUntilDate,
  getBorrowDateValidity,
  getFinishedBorrowDateValidity,
  computeValidityDuration,
  validateAdmin
}