<!-- omit in toc -->
# Library System

A midterm project for the university course
`Software Development with Quality Assurance`

<br/>

---

<!-- omit in toc -->
## Contents

- [Requirements](#requirements)
- [Dependencies](#dependencies)
- [Containerized Development](#containerized-development)
- [Standalone Development](#standalone-development)
- [Production build](#production-build)
- [Credentials](#credentials)
- [Environment Variables](#environment-variables)
- [Screenshots](#screenshots)

<br/>

---

## Requirements

1. [NodeJS](https://nodejs.org/en/) version 16.6 or higher

<br/>

---

## Dependencies

1. [Library System API](https://gitlab.com/yvnbunag/library-system-api)

<br/>

---

## Containerized Development

See [Library System Stack](https://gitlab.com/yvnbunag/library-system-stack)
repository

<br/>

---

## Standalone Development

1. Install dependencies
    ```sh
    yarn install
    ```
2. Set up environment variables
    ```sh
    # Copy pre configured env file for local development
    cp .env.local .env
    ```

    **OR**

    ```sh
    # Copy base env file then populate
    cp .env.dist .env
    ```
3. Start the application on watch mode
    ```sh
    yarn dev
    ```

> You may now access the application in
> [http://localhost:3000](http://localhost:3000) or the value of the
> `APPLICATION_URL` environment variable

<br/>

---

## Production build

1. Install dependencies
    ```sh
    yarn install
    ```
2. Build the application
    ```sh
    yarn build
    ```
3. Remove dev dependencies
    ```sh
    yarn install --production
    ```
4. Start the application
    ```sh
    yarn start
    ```

> When deploying, all source code needs to be included in the package

<br/>

---

## Credentials

To log in to the application, see credentials configuration in the
[Library System API](https://gitlab.com/yvnbunag/library-system-api#credentials)
documentation

<br/>

---

## Environment Variables

1. APPLICATION_URL
   - URL of the web application
2. API_URL
   - URL of the
     [Library System API](https://gitlab.com/yvnbunag/library-system-api)
3. API_APPLICATION_TOKEN
   - Token used to authorize the web application back-end when communicating
     with the API
   - Currently implemented as a mock bearer token, used like a basic token
   - Should be the same value as the `API_ACCESS_REGISTERED_MOCK_TOKEN`
     environment variable of the API

<br/>

---

## Screenshots

**Login page**

![Login page](/documentation/1-login-page.png)

**Home page**

![Home page](/documentation/2-home-page.png)

**Borrow queue modal**

![Borrow queue modal](/documentation/3-borrow-queue.png)

**Borrow history modal**

![Borrow history modal](/documentation/4-borrow-history.png)

**Printed resources page**

![Printed resources page](/documentation/5-printed-resources.png)

**Digital items page**

![Digital items page](/documentation/6-digital-items.png)

**Resource management page**

![Resource management page](/documentation/7-resource-management.png)

**Borrow management page**

![Borrow management page](/documentation/8-borrow-management.png)
