const axios = require('axios')
const express = require('express')

const handler = (() => {
  const app = express()

  app.post('/login', async (request, response) => {
    try {
      const { host, ...headers } = request.headers
      const { API_URL, API_APPLICATION_TOKEN } = process.env
      const { data } = await axios.post(
        `${API_URL}/security/authenticate`,
        request.body,
        {
          headers: {
            ...headers,
            Authorization: `Bearer ${API_APPLICATION_TOKEN}`
          }
        }
      )

      return response.json(data)
    } catch (error) {
      const status = error && error.response && error.response.status
        ? error.response.status
        : 500
      const data = error && error.response && error.response.data
        ? error.response.data
        : { success: false, error: error.message }

      response.status(status).json(data)
    }
  })

  return app
})()

module.exports = { path: '/api', handler }
